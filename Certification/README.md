# Yanis LAAKAD  (2022-2023)

## Objectifs !! 

                                                               CERTIFICATION

*Aujourd'hui je vais pour présenter mes différents objectifs pour pouvoir poursuivre mes études*. 
 
*Tout d'abord je vais finir ma dernière année de BTS, ensuite je poursuiverais avec une année de Bachelor pour finalement terminer sur une Master*. 


*En dessous je vous ai mit les différentes « certification » que j'ai pu faire tout au long de mon année scolaire et celle encore en cours pour continuer à enrichir mon parcours*. 



# Certification en Cours : 

[DYMA](https://dyma.fr/developer/list/chapters/core) *Certification JavaScript*

[PIX](https://pix.fr/ ) *Certification de recherche d'informations* 

[CODE WARS](https://www.codewars.com/dashboard) *Certification d'HTML et de CSS*

[GREAT LEARNING](https://www.mygreatlearning.com/academy?ambassador_code=hpexitpopupnvintl&gl_source=hpexitpopup)  *Ce site à acces à beaucoup de formations sur différents sujet comme la cybersécurité, le dévelopement , les intelligences artificielles, data sciences ect...*

[W3SCHOOL](https://www.w3schools.com/) *Certification sur différents langages de programmation*


# Certification Fini : 

[FREECODECAMP](https://www.freecodecamp.org/learn ) *Certification sur le dévelopement Web et les Algorithmes JavaScript *

[OPENCLASSROOM](https://openclassrooms.com/fr/)*Certification Git et GitHub et Ligne de commande dans un terminal * 

[UDEMY](https://www.udemy.com/) *Certification Front-End Web developpement et Build Quizz App (JavaScript,HTML,CSS)*

# Futur :

Ces certification vont mettre très utile pour mon profil professionnelle et même pour mes connaissances car cela va pouvoir m'avancer plus rapidements sur les types de sujets que je vais étudier en cours plus tard, cela est aussi un plus pour le profil professionnelle car cela montre ma motivation et mon envie de progression.

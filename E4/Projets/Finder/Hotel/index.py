from flask import Flask 
from flask import request
from flask_cors import CORS
from flask_mysqldb import MySQL
from flask import jsonify
from flask import Response
import json

app = Flask(__name__)
CORS(app)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'yanisl'
app.config['MYSQL_PASSWORD'] = 'root'
app.config['MYSQL_DB'] = 'caraibe'
mysql = MySQL(app)


@app.route('/user/<id>', methods=['GET']) 
def ma_fonction(id): 
  con = mysql.connection
  con.row_factory = lambda cursor, row: row
  cur = con.cursor()
  cur.execute("select * from user")
  data=[]
  rows = cur.fetchall(); 
  for row in rows:
    data.append(list(row))
  if len(data) != 0:
    return jsonify(data)
  else:
    return json.dumps({'error':str(data[0])})
  conn.close()
if __name__ == '__main__':
    app.run(debug=True)
// Import des modules nécessaires pour les tests
import { expect } from 'chai';
import app from '../app';
import supertest from 'supertest';

// Définition des données de test
const userData = {
  email: 'johndoe@example.com',
  password: 'password123'
};

const productData = {
  name: 'T-shirt',
  price: 20
};

// Définition du test d'intégration
describe('Test d\'intégration', () => {
  let request;
  let user;
  let product;

  // Configurer l'environnement de test avant chaque test
  beforeEach(async () => {
    request = supertest(app); // Initialiser la demande HTTP

    // Créer un utilisateur de test
    user = await createUser(userData);

    // Créer un produit de test
    product = await createProduct(productData);
  });

  // Tester l'API de création d'utilisateur
  describe('POST /users', () => {
    it('devrait créer un utilisateur avec des données valides', async () => {
      const res = await request
        .post('/users')
        .send({ email: 'janedoe@example.com', password: 'password123' });

      expect(res.statusCode).to.equal(201);
      expect(res.body).to.be.an('object');
      expect(res.body.email).to.equal('janedoe@example.com');
    });

    it('ne devrait pas créer un utilisateur avec des données invalides', async () => {
      const res = await request
        .post('/users')
        .send({ email: 'invalidemail', password: 'password123' });

      expect(res.statusCode).to.equal(400);
      expect(res.body).to.be.an('object');
      expect(res.body.error).to.equal('Invalid email address');
    });
  });

  // Tester l'API de récupération de produit
  describe('GET /products/:id', () => {
    it('devrait récupérer un produit existant', async () => {
      const res = await request.get(`/products/${product.id}`);

      expect(res.statusCode).to.equal(200);
      expect(res.body).to.be.an('object');
      expect(res.body.name).to.equal(product.name);
      expect(res.body.price).to.equal(product.price);
    });

    it('devrait retourner une erreur pour un produit inexistant', async () => {
      const res = await request.get('/products/123');

      expect(res.statusCode).to.equal(404);
      expect(res.body).to.be.an('object');
      expect(res.body.error).to.equal('Product not found');
    });
  });
});

// Fonctions utilitaires pour créer des données de test
async function createUser(userData) {
  // Implémenter la création d'un utilisateur
}

async function createProduct(productData) {
  // Implémenter la création d'un produit
}

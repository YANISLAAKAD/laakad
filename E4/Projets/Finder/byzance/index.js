const express = require('express');
const { Pool } = require('pg');
const cors= require('cors'); 
const app = express();

const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'postgres',
  password: 'postgres',
  port: 5432,
});
app.use(cors());

app.get('/utilisateur/:id', (req, res) => {
  const utilisateurId = req.params.id;
  pool.query('SELECT * FROM utilisateur WHERE id=$1', [utilisateurId], (error, results) => {
    if (error) {
      throw error;
    }
    res.status(200).json(results.rows);
  });
});

app.listen(8080, () => {
  console.log('Serveur démarré sur le port 8080');
});



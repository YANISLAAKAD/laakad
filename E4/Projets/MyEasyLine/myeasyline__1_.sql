-- Adminer 4.8.1 MySQL 5.7.36 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `bagage`;
CREATE TABLE `bagage` (
  `id_bagage` int(11) NOT NULL,
  `taille` decimal(15,2) DEFAULT NULL,
  `poids` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`id_bagage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `bagage` (`id_bagage`, `taille`, `poids`) VALUES
(1,	34.50,	25.00);

DROP TABLE IF EXISTS `tarifsvol`;
CREATE TABLE `tarifsvol` (
  `id_vols` int(11) NOT NULL,
  `id_tarifs` int(11) NOT NULL,
  `tarifs` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`id_vols`,`id_tarifs`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tarifsvol` (`id_vols`, `id_tarifs`, `tarifs`) VALUES
(1,	1,	67.98);

DROP TABLE IF EXISTS `vols`;
CREATE TABLE `vols` (
  `id_vols` int(11) NOT NULL AUTO_INCREMENT,
  `place` int(11) DEFAULT NULL,
  `compagnie` varchar(50) DEFAULT NULL,
  `dateVol` varchar(50) DEFAULT NULL,
  `destination` varchar(50) DEFAULT NULL,
  `heure` time DEFAULT NULL,
  `id_Voyageur` int(11) NOT NULL,
  PRIMARY KEY (`id_vols`),
  KEY `id_Voyageur` (`id_Voyageur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `vols` (`id_vols`, `place`, `compagnie`, `dateVol`, `destination`, `heure`, `id_Voyageur`) VALUES
(1,	20,	'easyline',	'2021/12/11',	'paris',	'18:00:00',	1);

DROP TABLE IF EXISTS `voyageur`;
CREATE TABLE `voyageur` (
  `id_Voyageur` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `DateNais` varchar(50) DEFAULT NULL,
  `codePostale` int(11) DEFAULT NULL,
  `rue` varchar(50) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `id_bagage` int(11) NOT NULL,
  PRIMARY KEY (`id_Voyageur`),
  KEY `id_bagage` (`id_bagage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `voyageur` (`id_Voyageur`, `nom`, `DateNais`, `codePostale`, `rue`, `ville`, `id_bagage`) VALUES
(1,	'baptiste',	'12/07/2003',	95000,	'moulin',	'cergy',	1),
(2,	'nolan',	'12/07/2005',	95000,	'puy',	'cergy',	1),
(3,	'alex',	'12/07/2000',	95000,	'four',	'cergy',	1);

-- 2022-12-08 07:44:19

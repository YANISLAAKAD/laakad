package javaClasse;

public class Voyageur {
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    protected  String nom;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    protected String date ;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;

    }

    protected   int numero;

    public Bagage getBagage() {
        return bagage;
    }

    public void setBagage(Bagage bagage) {
        this.bagage = bagage;
    }

    protected Bagage bagage;

    public int getCodePostale() {
        return codePostale;
    }

    public String getRue() {
        return rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public void setCodePostale(int codePostale) {
        this.codePostale = codePostale;
    }

    protected int codePostale;
    protected String rue;
    protected String ville;


public Voyageur ( String nom, String date , int numero,Bagage bagage,String codePostale,String ville,String rue) {
    this.nom = nom;
    this.numero = numero;
    this.date = date ;
    this.bagage = bagage;
}
public  Voyageur () {}

    @Override
    public String toString() {
        return "Voyageur{" +
                "nom='" + nom + '\'' +
                ", date='" + date + '\'' +
                ", numero=" + numero +
                ", bagage=" + bagage +
                ", codePostale='" + codePostale + '\'' +
                ", rue='" + rue + '\'' +
                ", ville='" + ville + '\'' +
                '}';
    }
}

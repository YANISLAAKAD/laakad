package javaClasse;
import java.sql.*;
import java.util.ArrayList;

public class vol {


    public int getNombrePassager() {
        return NombrePassager;
    }

    public void setNombrePassager(int nombrePassager) {
        NombrePassager = nombrePassager;
    }

    protected int NombrePassager;

    public String getCompagnie() {
        return Compagnie;
    }

    public void setCompagnie(String compagnie) {
        Compagnie = compagnie;
    }

    protected String Compagnie;

    public int getNumVol() {
        return NumVol;
    }

    public void setNumVol(int numVol) {
        NumVol = numVol;
    }

    protected int NumVol;

    public ArrayList<Voyageur> getListVoyageur() {
        return ListVoyageur;
    }

    public void setListVoyageur(ArrayList<Voyageur> listVoyageur) {
        ListVoyageur = listVoyageur;
    }

    protected ArrayList<Voyageur> ListVoyageur ;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    protected String date;

    public String getDestination() {
        return destination;
    }

    public void setLieu(String destination) {
        this.destination = destination;
    }

    protected  String destination;

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    protected String heure;

    public Double getTarif() {
        return tarif;
    }

    public void setTarif(Double tarif) {
        this.tarif = tarif;
    }

    protected Double tarif;
    public vol() {}
public vol( int NombrePassager, String Compagnie , int NumVol, ArrayList <Voyageur> List, String date,String destination,String heure)
{
    this.ListVoyageur = List;

    this.NombrePassager=NombrePassager;
    this.Compagnie = Compagnie;
    this.NumVol = NumVol;
    this.date =date;
    this.destination = destination;
    this.heure =  heure;
}
    @Override
    public String toString() {
        return "vol{" +
                "NombrePassager=" + NombrePassager +
                ", Compagnie='" + Compagnie + '\'' +
                ", NumVol=" + NumVol +
                ", ListVoyageur=" + ListVoyageur +
                ", date='" + date + '\'' +
                ", destination='" + destination + '\'' +
                ", heure='" + heure + '\'' +
                ", tarif=" + tarif +
                '}';
    }
}

package javaClasse;
import java.util.InputMismatchException;
public class Bagage
{
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {

        if (numero>0){this.numero = numero;}
        else {
            System.out.println("ce numéro doit être supérieur à 0");
        }
    }

    protected int numero;

    public double getTaille() {
        return taille;
    }

    public void setTaille(double taille) {
        if(taille>0) {this.taille = taille;}
        else {
            System.out.println("la taille entrer doit être supérieur à 0");
        }
    }

    protected double taille;

    public double getPoids() {
        return poids;
    }

    public void setPoids(double poids) {
       if(poids>0) {this.poids = poids;}
       else {
           System.out.println("le poids entrer doit être supèrieur à 0");
       }
    }

    protected  double poids;

    public Bagage(int numero, double taille, double poids)throws InputMismatchException{
        this.taille = taille ;
        this.numero = numero;
        this.poids = poids;
    }

    @Override
    public String toString() {
        return "Bagage{" +
                "numero=" + numero +
                ", taille=" + taille +
                ", poids=" + poids +
                '}';
    }

    public  Bagage() {}

    public static void main(String[] args) {
        Bagage b1 =new Bagage();
        b1.setNumero(12);
        b1.setPoids(-98);
        b1.setTaille(117);
        System.out.println(b1.toString());
       
    }
}


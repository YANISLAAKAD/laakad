package fr.hautil;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class AjouteVoyageurController {
    @FXML
    TextField nom;
    @FXML
    TextField prenom;
    @FXML
    TextField adresse;
    @FXML
    TextField anneeNaiss;
    @FXML
    TextField ville;
    @FXML
    TextField cp;



    public void btAnnuler(ActionEvent actionEvent) {
        nom.setText("");
        prenom.setText("");
        adresse.setText("");
        anneeNaiss.setText("");
        ville.setText("");
        cp.setText("");

    }
    public void btValider(ActionEvent actionEvent) {
        System.out.println(nom.getText());
        System.out.println(prenom.getText());
        System.out.println(adresse.getText());
        System.out.println(anneeNaiss.getText());
        System.out.println(ville.getText());
        System.out.println(cp.getText());

    }
}

module fr.hautil.mission7 {
    requires javafx.controls;
    requires javafx.fxml;


    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;

    opens fr.hautil.mission7 to javafx.fxml;
    exports fr.hautil.mission7;
}
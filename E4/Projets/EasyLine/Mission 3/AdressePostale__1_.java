public class AdressePostale { 

    private static String numVoie;
    private static String ville;
    private static String cp; 

    public AdressePostale(){
    }
    public AdressePostale(String numVoie,String ville,String cp){
            this.numVoie=numVoie;
            this.ville=ville;
            this.cp=cp;
        
        public void setNumvoie(String numVoieV){
            numVoie = numVoieV;
        } 
        public void setVille(String villeV){
            ville = villeV;
        } 
        public void setCp(String cpV){
            cp = cpV;
        }
        public static String getNumVoie()
        {
        return numVoie;
        } 
        public String getVille()
        {
        return ville;
        } 
        public String getCp()
        {
            return cp;
        }
        public void afficher(){
            System.out.println(this.numVoie);
            System.out.println(this.ville);
            System.out.println(this.cp);
        }
    
    }
}